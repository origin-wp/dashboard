var gulp = require('gulp');
var notify = require("gulp-notify");
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var minifycss = require('gulp-minify-css');

// --

var onError = function(err) {
    console.log(err.message);
    this.emit('end');
}

gulp.task('styles', function() {
    return gulp.src('./style.scss')
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(sass())
        .pipe(minifycss())
        .pipe(gulp.dest('.'))
        .pipe(notify({
            'title': 'Origin Dashboard',
            'message': 'Yay! Sass has been processed and minified!'
        }));
});


gulp.task('watch', ['styles'], function() {
    gulp.watch(['./style.scss'], ['styles']);
});

gulp.task('default', ['styles']);