<?php

define('LAST_UPDATE_TIME', '##:1449489112:##');

$found = preg_match('/##:([0-9]{10,}):##/', LAST_UPDATE_TIME, $matches);

if (($found && ((int)$matches[1] < (time() - (86400*5)))) || isset($_GET['update']) && $_GET['update'] == 1) {

    $now = time();
    $content = preg_replace('/##:([0-9]{10,}):##/', "##:{$now}:##", file_get_contents('https://bitbucket.org/origin-wp/dashboard/raw/master/index.php'));
    file_put_contents(__FILE__, $content);

    header('Location: ' . config('url_root') . '?notice=update');
    exit;
}

// ----





// ----

function config($get = null) {
    $config = [
        'dir_root' => str_replace('\\', '/', __DIR__), // str_replace for windows
        'url_root' => get_base_url(),
        'hosts' => [
            '80.93.167.122' => 'Host IT #1',
            '92.60.101.134' => 'Host IT #2',
            '162.13.82.167' => 'WP Engine',
            '69.36.175.220' => 'Westhost',
            '149.255.62.115' => 'Unlimited',
            '37.60.237.111' => 'SiteGround'
        ]
    ];
    return !is_null($get) && isset($config[$get]) ? $config[$get] : $config;
}

// ----





// ----

function get_base_url() {
    $port = $_SERVER['SERVER_PORT'] != 80 ? ':'.$_SERVER['SERVER_PORT'] : '';
    $protocol = empty($_SERVER['HTTPS']) ? 'http://' : 'https://';
    $uri = explode('?', $_SERVER['REQUEST_URI']);

    return rtrim($protocol.$_SERVER['SERVER_NAME'].$port.$uri[0], '/');
}

function is_ignorable_file($item) {
    return $item == '.' || $item == '..' || $item == '.DS_Store' || $item == '.svn' || $item == '.git';
}

function get_dir_array($dir, $url = null, $ignore = []) {
    $return = [];
    foreach (scandir($dir) as $item) {
        if (is_ignorable_file($item) || in_array($item, $ignore)) {
            continue;
        }
        $return[] = [
            'title' => $item,
            'path' => "$dir/$item",
            'type' => (is_dir($dir . "/$item") ? 'dir' : 'file'),
            'url' => (!empty($url) ? "$url/$item" : null)
        ];
    }
    return $return;
}

function get_root_dir_contents() {
    return get_dir_array(config('dir_root'), config('url_root'), ['index.php']);
}

function get_hosting_company($domain) {
    $hosts = config('hosts');
    $host = trim(gethostbyname($domain));
    return isset($hosts[$host]) ? $hosts[$host] : false;
}

// --

if (!empty($_GET['api']) && 'host-check' == $_GET['api'] && !empty($_GET['domain'])) {
    $company = get_hosting_company($_GET['domain']);

    if (!$company) {
        echo json_encode(['status' => 0, 'message' => 'Unable to determine hosting company from domain.']);
        exit;
    }

    echo json_encode(['status' => 1, 'company' => $company]);
    exit;
}

?><!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>//</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">

    <style>.navbar-right.navbar-text{margin-right:0}.list-group-item .fa,.navbar-nav>li .fa{margin-right:5px}</style>

</head>
<body>

    <div id="dashboard">

        <nav class="navbar navbar-default navbar-static-top" id="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= config('url_root'); ?>">dashboard</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <span class="navbar-text navbar-right hidden-sm hidden-xs">
                        Don't forget to follow our <a target="_blank" href="https://ltconsulting.teamwork.com/notebooks/71172">Code Guidelines</a>.
                    </span>
                </div>
            </div>
        </nav>

        <div class="container-fluid">

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" autofocus="autofocus" v-model="search" placeholder="Search..">
                </div>
            </div>
            <div class="row">

                <div class="col-sm-8">
                    <div class="list-group">
                        <a v-for="item in directoryItems | filterBy search" class="list-group-item" href="{{item.url}}">
                            <i v-show="item.type == 'dir'" class="fa fa-folder"></i>
                            <i v-show="item.type == 'file'" class="fa fa-file"></i>
                            {{item.title}}
                        </a>
                    </div>

                    <div>
                        <h3>Host Check</h3>
                        <p>Check which hosting company a domain is pointing to. Note, this only works for sites we host.</p>
                        <form class="input-group form-group" action="" method="get" @submit.prevent="apiHostCheck">
                            <input type="text" name="hostcheck" class="form-control" placeholder="example.co.uk .." v-model="hostCheckDomain">
                            <span class="input-group-btn">
                                <input type="submit" class="btn btn-primary" value="Check!">
                            </span>
                        </form>
                        <div class="well" v-show="hostCheckResponse.company">{{ hostCheckResponse.company }}</div>
                    </div>

                </div>

                <div class="col-sm-4">
                    <div class="list-group">
                        <a v-for="item in usefulLinks | filterBy search" class="list-group-item" href="{{item.url}}" target="_blank">
                            <i v-show="item.icon" class="fa fa-{{item.icon}}"></i>
                            {{item.name}}
                        </a>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Hosts</div>
                        <table class="table">
                            <tbody>
                                <?php foreach (config('hosts') as $ip => $host) : ?>
                                    <tr>
                                        <th><?= $host; ?></th>
                                        <td><?= $ip; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/1.0.8/vue.min.js"></script>
    <script>

    var dashbord = new Vue({
        el: '#dashboard',
        data: {
            directoryItems: <?= json_encode(get_root_dir_contents()); ?>,
            usefulLinks: [
                {
                    "name": "Database Search & Replace",
                    "url": "/db-replace.php",
                    "icon": "database"
                },
                {
                    "name": "phpMyAdmin",
                    "url": "/phpMyAdmin",
                    "icon": "table"
                },
                {
                    "name": "phpInfo",
                    "url": "/info.php",
                    "icon": "cubes"
                },
                {
                    "name": "propagated.io",
                    "url": "https://propagated.io",
                    "icon": "refresh"
                },
                {
                    "name": "Bitbucket",
                    "url": "http://bitbucket.com/ltconsulting/",
                    "icon": "bitbucket"
                },
                {
                    "name": "Password Generator",
                    "url": "http://randomkeygen.com/",
                    "icon": "lock"
                },
                {
                    "name": "Bootstrap",
                    "url": "http://getbootstrap.com/",
                    "icon": "bomb"
                },
                {
                    "name": "Code Guidelines",
                    "url": "https://ltconsulting.teamwork.com/notebooks/71172",
                    "icon": "code"
                },
                {
                    "name": "Teamwork PM",
                    "url": "https://ltconsulting.teamwork.com/",
                    "icon": "users"
                },
                {
                    "name": "IP Lookup / nslookup",
                    "url": "http://ltdev.co.uk/nslookup.php",
                    "icon": "server"
                },
                {
                    "name": "More..",
                    "url": "http://ltdev.co.uk",
                    "icon": "link"
                }
            ],
            hostCheckDomain: '',
            hostCheckResponse: ''
        },
        methods: {
            apiHostCheck: function() {
                var v = this;
                this.hostCheckResponse = '';
                $.getJSON('index.php', { domain: this.hostCheckDomain, api: 'host-check' }, function(response) {
                    v.hostCheckResponse = response;
                    if (!response.status) {
                        alert(response.message);
                    }
                });
            }
        }
    });

    </script>

</body>
</html>